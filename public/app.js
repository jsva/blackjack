const cardValues = {};

const playerHand = [];

const dealerHand = [];

let dealerHandHidden = [];

let deckId = "";

let remainingCards = 0;

let hiddenCard = { value: 0, image: "" };

let decksUsed = 6; //Default value, if anyone gets a deck with other size, it will be overwritten.

const getDeck = () => {
  // Gets a new deck from the API of the size specified
  let amountOfDecks = 6;

  amountOfDecks = parseInt(prompt("How many decks do you want to play with?"));
  decksUsed = amountOfDecks;
  const link =
    "https://www.deckofcardsapi.com/api/deck/new/shuffle/?deck_count=" +
    amountOfDecks;
  fetch(link)
    .then((response) => response.json())
    .then((data) => parseDeck(data))
    .catch((error) => console.log(error));

  //Update DOM
};

const parseDeck = (data) => {
  // saves the data from the received deck
  if (!data.success) {
    alert(
      "Error retrieving deck, try again. If error persists try again at a later time"
    );
    return;
  }
  deckId = data.deck_id;
  console.log(`Received deck with id: ${deckId}`);
  remainingCards = data.remaining;
  updateCards();
  updateDecksUsed();
};

const shuffleDeck = () => {
  //shuffles the deck again, it adds back all used cards before the shuffle.
  const link =
    "https://www.deckofcardsapi.com/api/deck/" + deckId + "/shuffle/";
  fetch(link)
    .then((response) => response.json())
    .then((data) => {
      if (!data.success) {
        alert("Shuffling failed try again");
      } else {
        console.log("Shuffled the deck");
        remainingCards = data.remaining;
        cardsDrawn = 0;
        //deckId = data.deck_id; deck_id stays the same when shuffling a deck
      }
    })
    .catch((error) => console.log(error));
};

const drawCard = async (target) => {
  const link =
    "https://www.deckofcardsapi.com/api/deck/" + deckId + "/draw/?count=1";
  card = {};
  await fetch(link)
    .then((response) => response.json())
    .then((data) => {
      if (data.success) {
        // console.log(data.cards[0])
        // console.log(data.cards[0].value)
        // console.log(data.cards[0].image)
        card.value = data.cards[0].value;
        card.image = data.cards[0].image;
        remainingCards = data.remaining;
        // console.log(`Drew: ${data.cards[0].code}`)
      }
    })
    .catch((error) => console.log(error));
  updateCards();
  // console.log(card);
  return card;
};

const startNewGame = async () => {
  resetGame();
  updateDecksUsed();
  updateCards();
  updateStatus("New game!");
  const dealerCard1 = await drawCard(dealerHand);
  console.log("Dealer drew: " + dealerCard1.value);
  dealerHand.push(dealerCard1.value);
  dealerCard1Element.src = dealerCard1.image;
  dealerValueElement.innerText = calculateHand(dealerHand);

  await playerDraw();

  const dealerCard2 = await drawCard();
  hiddenCard = dealerCard2;

  await playerDraw();

  if (isBlackJack(playerHand)) {
    console.log("player got blackjack");
    updateStatus("player got blackjack");
    dealerPlay();
  }
  updateStatus("Cards dealt. Player choose to hit or stay");
};

const playerDraw = async () => {
  if (!isBust(playerHand)) {
    updateStatus("Player hit!")
    const cardInfo = await drawCard();
    console.log("Got: " + cardInfo.value);
    playerHand.push(cardInfo.value);
    addCardImage(cardInfo, playerCardsSpanElement);
  } else {
    alert("You've lost start a new hand!");
  }
  playerScoreSpanElement.innerText = calculateHand(playerHand);
  console.log("Player has " + calculateHand(playerHand));
  if (isBust(playerHand)) {
    console.log("player bust");
    updateStatus("Player bust");
  }
};

const playerStay = () => {
  updateStatus("Player stay! Dealers turn:")
  dealerPlay();
};

const dealerPlay = async () => {
  dealerHand.push(hiddenCard.value);
  dealerCard2Element.src = hiddenCard.image;
  dealerValueElement.innerText = calculateHand(dealerHand);
  if (isBlackJack(dealerHand)) {
    console.log("Dealer got blackjack");
    updateStatus("Dealer got blackjack");
    if (isBlackJack(playerHand)) {
      console.log("Player also got blackjack, we push!");
      updateStatus("Player also got blackjack, we push!");
    } else {
      console.log("Dealer won");
      updateStatus("Dealer won");
    }
  } else if (isBlackJack(playerHand)) {
    console.log("Player got blackjack, and won!");
    updateDecksUsed("Player Got blackjack and won")
  } else {
  while (calculateHand(dealerHand) < 17 && !isBust(dealerHand)) {
    const cardInfo = await drawCard();
    dealerHand.push(cardInfo.value);
    addCardImage(cardInfo, dealerCardsSpanElement);
    dealerValueElement.innerText = calculateHand(dealerHand);
  }
  if (isBust(dealerHand)) {
    console.log("Dealer bust, player won");
    updateStatus("Dealer bust, player won");
  } else {
    console.log("Dealer stays");
    updateStatus("Dealer stays!");
    console.log(determineWinner());
    updateStatus(determineWinner());
  }
}};

const getCardValue = (card) => {
  switch (card) {
    case "ACE":
      return 11;
    case "KING":
      return 10;
    case "QUEEN":
      return 10;
    case "JACK":
      return 10;
    default:
      return parseInt(card);
  }
};

const calculateHand = (hand) => {
  let handTotal = 0;
  let ace = 0;
  hand.forEach((card) => {
    // console.log('Evaluating ' + card)
    value = getCardValue(card);
    if (value == 11) {
      ace++;
    }
    // console.log('Card has value: ' + value)
    // console.log('Value has type: ' + typeof value)
    handTotal += value;
  });
  if (handTotal > 21) {
    while (ace) {
      ace--;
      handTotal -= 10;
    }
  }
  return handTotal;
};

const isBlackJack = (hand) => {
  if (hand.length != 2 || calculateHand(hand) != 21) {
    return false;
  }
  return true;
};

const isBust = (hand) => {
  return calculateHand(hand) > 21;
};

const determineWinner = () => {
  // should only be called if both players did not bust nor get blackjack
  if (calculateHand(playerHand) == calculateHand(dealerHand)) {
    return "push";
  } else if (calculateHand(playerHand) > calculateHand(dealerHand)) {
    return "Player won";
  } else {
    return "Dealer won";
  }
};

const resetGame = () => {
  playerHand.length = 0;
  dealerHand.length = 0;
  hiddenCard = { value: 0, image: "" };

  dealerCard1Element.src = "https://deckofcardsapi.com/static/img/back.png";
  dealerCard2Element.src = "https://deckofcardsapi.com/static/img/back.png";
  dealerCard1Element.alt = "";
  dealerCard2Element.alt = "";

  playerCardsSpanElement.innerHTML = "";
  dealerCardsSpanElement.innerHTML = "";
};

//DOM ELEMEENTS

const startButtonElement = document.getElementById("start");
const shuffleButtonElement = document.getElementById("shuffle");
const startNewGameElement = document.getElementById("new-hand");
const hitButtonElement = document.getElementById("hit");
const stayButtonElement = document.getElementById("stay");

const dealerCard1Element = document.getElementById("dealer-card-1");
const dealerCard2Element = document.getElementById("dealer-card-2");
const dealerCardsSpanElement = document.getElementById("dealer-card-span");
const dealerValueElement = document.getElementById("dealer-value");

const playerCardsSpanElement = document.getElementById("player-cards");
const playerScoreSpanElement = document.getElementById("player-score");

const statusElement = document.getElementById("status-message");
const previousStatusElement = document.getElementById("previous-message");

const cardsLeftElement = document.getElementById("cards-left");
const cardsDrawnElement = document.getElementById("cards-drawn");
const decksUsedElement = document.getElementById("decks-used");

//DOM MANIP/UPDATERS

const updateStatus = (message) => {
  previousStatusElement.innerText = statusElement.innerText;
  statusElement.innerText = message;
};

const updateCards = () => {
  cardsLeftElement.innerText = remainingCards;
  cardsDrawnElement.innerText = decksUsed * 52 - remainingCards;
};

const updateDecksUsed = () => {
  decksUsedElement.innerText = decksUsed;
};

const addCardImage = (cardInfo, parentElement) => {
  const imageElement = document.createElement("img");
  imageElement.src = cardInfo.image;
  imageElement.alt = cardInfo.value;
  imageElement.height = "200";
  imageElement.width = "125";
  parentElement.appendChild(imageElement);
};

//EVENT LISTENERS
startButtonElement.addEventListener("click", getDeck);
shuffleButtonElement.addEventListener("click", shuffleDeck);
hitButtonElement.addEventListener("click", playerDraw);
startNewGameElement.addEventListener("click", startNewGame);
stayButtonElement.addEventListener("click", playerStay);
